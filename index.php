<?php
  require_once('connection.php');
  session_start();
  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
    $controller = 'pages';
    $action     = 'home';
  }
  if ($action == 'login' || $action == 'register' || $action == 'profile' || $action == 'about'){
  	require_once('routes.php');
  } else {
  	require_once('views/layout.php');
  }
?>