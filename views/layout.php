<DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>About Me+</title>
    <link rel="icon" href="views/assets/img/favicon.png" type="image/x-icon"/>
    <link href="views/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="views/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="views/assets/css/animate.css" rel="stylesheet">
    <link href="views/assets/css/style.css" rel="stylesheet">
    <link href="views/assets/css/custom.css" rel="stylesheet">

</head>
  <body>
    <?php
      if (isset($_SESSION['user_infor'])){
        $user = $_SESSION['user_infor'];
        if (empty($user['HinhAnh'])){
          $user['HinhAnh'] = "views/assets/img/no-avatar.png";
        }
      } else {
        // die("<script>window.location.href = 'http://localhost/ltweb_aboutme/index.php?controller=users&action=login';</script>");
      }
    ?>
<div id="wrapper">
  <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
          <ul class="nav metismenu" id="side-menu">
              <li class="nav-header">
                  <div class="dropdown profile-element"> 
                      <div class="avatar-frame">
                          <img alt="image" class="img-circle" src="<?php echo $user['HinhAnh'] ?>" />
                      </div>
                   
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $user['HoTen'] ?></strong>
                      </a>
                      <ul class="dropdown-menu animated fadeInRight m-t-xs">
                          <li><a href="index.php?controller=users&action=profile">Profile</a></li>
                          <li><a href="#">Liên hệ</a></li>
                          <li class="divider"></li>
                          <li><a href="index.php?controller=users&action=logout">Logout</a></li>
                      </ul>
                  </div>
                  <div class="logo-element">
                      ME+
                  </div>
              </li>
              
              <li>
                  <a href="index.php?controller=users&action=preview"><i class="fa fa-th-large"></i>
                  <span class="nav-label">Profile</span></a>
              </li>
              <li>
                  <a href="index.php?controller=users&action=edit"><i class="fa fa-pencil-square-o"></i>
                  <span class="nav-label">Edit profile</span></a>
              </li>
              <li>
                  <a href="index.php?controller=users&action=profile&me=<?php echo $user['Id'] ?>"><i class="fa fa-desktop"></i>
                  <span class="nav-label">Your page</span></a>
              </li>
          </ul>
      </div>
  </nav>

  <div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
      <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome <?php echo $user['HoTen'] ?></span>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning"></span>
                    </a>
                  
                </li>
                


                <li>
                    <a href="index.php?controller=users&action=logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>

    <?php require_once('routes.php'); ?>

    <div class="footer">
        <div class="pull-right">
            <a href="index.php?controller=pages&action=about">About us</a>
        </div>
        <div>
            <strong>Copyright</strong> Bắp&amp;Rốt &copy; 2016-2017
        </div>
    </div>
  </div>
</div>



    <!-- Mainly scripts -->
    <script src="views/assets/js/jquery-2.1.1.js"></script>
    <script src="views/assets/js/bootstrap.js"></script>
    <script src="views/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="views/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="views/assets/js/inspinia.js"></script>
    <script src="views/assets/js/plugins/pace/pace.min.js"></script>

    <!-- Peity -->
    <script src="views/assets/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Peity -->
    <script src="views/assets/js/demo/peity-demo.js"></script>
    <script src="views/assets/js/custom.js"></script>
  <body>
<html>