<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.2/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jul 2015 09:59:55 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>ABOUT US+</title>
        <link rel="icon" href="views/assets/img/favicon.png" type="image/x-icon"/>
		<!-- Bootstrap core CSS -->
		<link href="views/assets/css/custom.css" rel="stylesheet">
		<link href="views/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="views/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

		<link href="views/assets/css/animate.css" rel="stylesheet">
		<link href="views/assets/css/style.css" rel="stylesheet">
		
</head>
    
<body id="page-top" class="landing-page">
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top navbar-scroll" role="navigation" style="border-bottom: 1px solid #e7eaec !important;">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php?controller=users&action=login">YOUR PAGE</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                                <li><a class="page-scroll" href="#aboutus">ABOUT US</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<section id="aboutus" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Our Team</h1>
                <p>IT Faculty - Da Nang University of Science and Technology</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 wow fadeInLeft">
                <div class="team-member">
                    <img src="views/assets/img/huu.jpg" class="avatar-team img-responsive img-circle" alt="" style="width: 260px;">
                    <h4>Truong <span class="navy"> Thanh Huu</span> </h4>
                    <p>13T4 - ITF - DUT</p>
                    <p>Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma which is living with the results of other people's thinking. Don't let the noise of others' opinions drown out your own inner voice. And most important, have the courage to follow your heart and intuition. </p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.facebook.com/truongthanhhhuu" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 wow fadeInRight">
                <div class="team-member">
                    <img src="views/assets/img/landing/avatar2.jpg" class="img-responsive img-circle" alt="">
                    <h4>Le<span class="navy"> Thi Thanh Nga</span> </h4>
                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus.</p>
                    <ul class="list-inline social-icon">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p>Bài tập lớn môn học Công nghệ web - GVHD: Mai Văn Hà</p>
            </div>
        </div>
    </div>
</section>

<!-- Mainly scripts -->
<script src="views/assets/js/jquery-2.1.1.js"></script>
<script src="views/assets/js/bootstrap.min.js"></script>
<script src="views/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="views/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="views/assets/js/inspinia.js"></script>
<script src="views/assets/js/plugins/pace/pace.min.js"></script>
<script src="views/assets/js/plugins/wow/wow.min.js"></script>


<script>

    $(document).ready(function () {

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                            scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
        });
    });

    var cbpAnimatedHeader = (function() {
        var docElem = document.documentElement,
            header = document.querySelector( '.navbar-default' ),
            didScroll = false,
            changeHeaderOn = 0;
        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }
        function scrollPage() {
            var sy = scrollY();
            if ( sy >= changeHeaderOn ) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();

    })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();

</script>

</body>

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.2/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jul 2015 10:00:56 GMT -->
</html>
