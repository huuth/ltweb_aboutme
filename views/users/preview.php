<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index-2.html">Home</a>
            </li>
            <li class="active">
                <strong>Profile</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<section id="team" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <!--    <h1>Trương Thanh Hữu</h1>
                <p>Trường Đại học Bách khoa Đà Nẵng</p> -->
            </div>
        </div>
        <?php
            $me  = $_SESSION['user_infor'];
        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member wow zoomIn">
                    <?php 
                        
                        $url_pic = $me['HinhAnh'];
                        if(empty($url_pic)){
                            echo '<img src="views/assets/img/no-avatar.png" class="img-responsive img-custom" alt="">';
                        }else{?>
                            <img src="<?php echo $url_pic; ?>" class="img-responsive img-custom" alt="">
                        <?php } ?>
                    
                    <h4><span class="navy">Contact me</span></h4>
                    <p></p>
                    <ul class="list-inline social-icon">
                                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                                    </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8">
                
                <h1 class="text-navy name-custom"><?php echo $me['HoTen']?></h1>
                <hr>
                <dl class="dl-horizontal">
                                <div class="info-line"><dt>Làm việc/học tập:</dt> <dd><?php echo $me['CoQuan']?></dd></div>
                                <div class="info-line"><dt>Chức vụ:</dt> <dd><?php echo $me['ChucVu']?></dd></div>
                                <div class="info-line"><dt>Email:</dt> <dd><?php echo $me['Email']?></dd></div>
                                <div class="info-line"><dt>Số điện thoại:</dt> <dd><?php echo $me['SoDienThoai']?></dd></div>
                                <div class="info-line"><dt>Địa chỉ:</dt> <dd><?php echo $me['DiaChi']?></dd></div>
                                <div class="info-line"><dt>Kỹ năng:</dt> <dd><?php echo $me['KyNang']?></dd></div>
                                <div class="info-line"><dt>Sở thích:</dt> <dd><?php echo $me['SoThich']?></dd></div>
                </dl>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                    <p>-- Châm ngôn --</p>
                    <p><?php echo $me['ChamNgon'] ?></p> 
                </div>
            </div>
        </div>
    </div>
</section>