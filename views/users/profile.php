﻿<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.2/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jul 2015 09:59:55 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>ABOUT ME+</title>
    <link rel="icon" href="views/assets/img/favicon.png" type="image/x-icon"/>
		<!-- Bootstrap core CSS -->
		<link href="views/assets/css/custom.css" rel="stylesheet">
		<link href="views/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="views/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

		<link href="views/assets/css/animate.css" rel="stylesheet">
		<link href="views/assets/css/style.css" rel="stylesheet">
		
</head>
    
<body id="page-top" class="landing-page">
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top navbar-scroll" role="navigation" style="border-bottom: 1px solid #e7eaec !important;">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php?controller=users&action=login">YOUR PAGE</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                                <li><a class="page-scroll" href="#team">PROFILE</a></li>
                                <!-- <li><a class="page-scroll" href="#album">ALBUM</a></li> -->
                                <li><a class="page-scroll" href="index.php?controller=pages&action=about">ABOUT US</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<section id="team" class="gray-section team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <!-- 	<h1>Trương Thanh Hữu</h1>
                <p>Trường Đại học Bách khoa Đà Nẵng</p> -->
            </div>
        </div>
        <?php
            $me  = $_SESSION['me'];
        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member wow zoomIn">
                    <?php 
                        
                        $url_pic = $me['HinhAnh'];
                        if(empty($url_pic)){
                            echo '<img src="views/assets/img/no-avatar.png" class="img-responsive img-custom" alt="">';
                        }else{?>
                            <img src="<?php echo $url_pic;?>" class="img-responsive img-custom" alt="">';
                        <?php } ?>
                    
                    <h4><span class="navy">Contact me</span></h4>
                    <p></p>
                    <ul class="list-inline social-icon">
                                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                                    </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-8">
                
                <h1 class="text-navy name-custom"><?php echo $me['HoTen']?></h1>
                <hr>
                <dl class="dl-horizontal">
                                <div class="info-line"><dt>Làm việc/học tập:</dt> <dd><?php echo $me['CoQuan']?></dd></div>
                                <div class="info-line"><dt>Chức vụ:</dt> <dd><?php echo $me['ChucVu']?></dd></div>
                                <div class="info-line"><dt>Email:</dt> <dd><?php echo $me['Email']?></dd></div>
                                <div class="info-line"><dt>Số điện thoại:</dt> <dd><?php echo $me['SoDienThoai']?></dd></div>
                                <div class="info-line"><dt>Địa chỉ:</dt> <dd><?php echo $me['DiaChi']?></dd></div>
                                <div class="info-line"><dt>Kỹ năng:</dt> <dd><?php echo $me['KyNang']?></dd></div>
                                <div class="info-line"><dt>Sở thích:</dt> <dd><?php echo $me['SoThich']?></dd></div>
                </dl>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                    <p>-- Châm ngôn --</p>
                    <p><?php echo $me['ChamNgon'] ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section id="album">
	<div class="container">
		<div class="ibox-title">
      <h2>Album</h2>
	  </div>
	 <div id="album-box" class="row">
      <div class="col-sm-6">
          <img alt="image"  class="img-responsive" src="views/assets/img/p_big2.jpg">
      </div>
      <div class="col-sm-6">
          <img alt="image"  class="img-responsive" src="views/assets/img/p_big3.jpg">
      </div>
      <div class="col-sm-6">
          <img alt="image"  class="img-responsive"  src="views/assets/img/p_big1.jpg">
      </div>
      <div class="col-sm-6">
          <img alt="image" class="img-responsive" src="views/assets/img/p_big2.jpg">
      </div>
  	</div>	
  </div>
</section> -->

<!-- Mainly scripts -->
<script src="views/assets/js/jquery-2.1.1.js"></script>
<script src="views/assets/js/bootstrap.min.js"></script>
<script src="views/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="views/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="views/assets/js/inspinia.js"></script>
<script src="views/assets/js/plugins/pace/pace.min.js"></script>
<script src="views/assets/js/plugins/wow/wow.min.js"></script>


<script>

    $(document).ready(function () {

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                            scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
        });
    });

    var cbpAnimatedHeader = (function() {
        var docElem = document.documentElement,
            header = document.querySelector( '.navbar-default' ),
            didScroll = false,
            changeHeaderOn = 0;
        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }
        function scrollPage() {
            var sy = scrollY();
            if ( sy >= changeHeaderOn ) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();

    })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();

</script>

</body>

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.2/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jul 2015 10:00:56 GMT -->
</html>
