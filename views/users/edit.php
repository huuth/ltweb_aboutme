<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index-2.html">Home</a>
            </li>
            <li class="active">
                <strong>Edit Profile</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<?php
  if (isset($_SESSION['user_infor'])){
    $user = $_SESSION['user_infor'];
    if (empty($user['HinhAnh'])){
          $user['HinhAnh'] = "views/assets/img/no-avatar.png";
        }
  } else
    die("<script>window.location.href = 'http://localhost/ltweb_aboutme/index.php?controller=users&action=login';</script>");
?>

<div class="ibox-content">
  <form action="index.php?controller=users&action=update" method="post" enctype='multipart/form-data' class="form-horizontal">
      <input name="id" value=<?php echo $user['Id'] ?> type="hidden" class="form-control">
      <div class="form-group"><label class="col-sm-2 control-label">Tên</label>
          <div class="col-sm-6"><input name="hoten" value="<?php echo $user['HoTen'] ?>" placeholder="Họ tên" type="text" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>

      <div class="form-group"><label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-6"><input name="email" value="<?php echo $user['Email'] ?>" type="email" placeholder="Email" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>
      
			<div class="form-group"><label class="col-sm-2 control-label">Số điện thoại</label>
          <div class="col-sm-6"><input name="sodienthoai" value="<?php echo $user['SoDienThoai'] ?>" type="number" placeholder="Số điện thoại" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>

      <div class="form-group"><label class="col-sm-2 control-label">Địa chỉ</label>
          <div class="col-sm-6"><input name="diachi" value="<?php echo $user['DiaChi'] ?>" type="text" placeholder="Địa chỉ" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>

      <div class="form-group"><label class="col-sm-2 control-label">Avatar</label>
        <div class="upload-img-box">
          <div class="col-sm-6">
            <input id="image_upload" type="file" name="hinhanh" accept="image/*" >
            <div id="image-upload" class="image-upload">
              <div class="thumb"><img src="<?php echo $user['HinhAnh'] ?>"></div>
            </div>
        </div>
      </div>
      <div class="hr-line-dashed"></div>

      <div class="form-group"><label class="col-sm-2 control-label">Cơ quan</label>
          <div class="col-sm-6"><input name="coquan" value="<?php echo $user['CoQuan'] ?>" type="text" placeholder="Cơ quan" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>

      <div class="form-group"><label class="col-sm-2 control-label">Chức vụ</label>
          <div class="col-sm-6"><input name="chucvu" value="<?php echo $user['ChucVu'] ?>" type="text" placeholder="Chức vụ" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>
      
      <div class="form-group"><label class="col-sm-2 control-label">Kĩ năng</label>
          <div class="col-sm-6"><input name="kynang" value="<?php echo $user['KyNang'] ?>" type="text" placeholder="Kĩ năng" class="form-control"></div>
      </div>
      <div class="hr-line-dashed"></div>

      <div class="form-group"><label class="col-sm-2 control-label">Sở thích</label>
          <div class="col-sm-6"><textarea name="sothich" type="text" placeholder="Sở thích" class="form-control"><?php echo $user['SoThich'] ?></textarea></div>
      </div>
      <div class="hr-line-dashed"></div>
      
      <div class="form-group"><label class="col-sm-2 control-label">Châm ngôn</label>
          <div class="col-sm-6"><textarea name="chamngon" value="" type="text" placeholder="Châm ngôn" class="form-control"><?php echo $user['ChamNgon'] ?></textarea></div>
      </div>
      <div class="hr-line-dashed"></div>

      

      <div class="form-group">
          <div class="col-sm-4 col-sm-offset-2">
              <!-- <button class="btn btn-white" type="submit">Cancel</button> -->
              <button class="btn btn-primary" type="submit">Save changes</button>
          </div>
      </div>
  </form>
</div>