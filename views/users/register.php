<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.2/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jul 2015 09:55:25 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>
    <link rel="icon" href="views/assets/img/favicon.png" type="image/x-icon"/>
    <link href="views/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="views/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="views/assets/css/animate.css" rel="stylesheet">
    <link href="views/assets/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">ME+</h1>
            </div>
            <h3>Welcome to About me+</h3>
            <p>Đăng kí tài khoản mới</p>
            <?php 
              $error = "";
              if (isset($_SESSION['error'])){
                $error = $_SESSION['error'];
                unset($_SESSION['error']);
              }
            ?>
            <div class="error"><p style="color: red;"><?php echo $error ?></p></div>
            <form method="post" action="index.php?controller=users&action=create" class="m-t form-horizontal" role="form" >
     <!--          <div class="form-group">
                <input type="text" name="hoten" class="form-control" placeholder="Fullname" required="">
              </div> -->
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" required="">
              </div>
              <div class="form-group">
                <input type="password" name="matkhau" class="form-control" placeholder="Mật khẩu" required="">
              </div>
              <div class="form-group">
                <input type="password" name="xacnhanmatkhau" class="form-control" placeholder="Xác nhận mật khẩu" required="">
              </div>
              <!-- <div class="form-group">
                <input type="number" name="sodienthoai" class="form-control" placeholder="Số điện thoại" required="">
              </div>
              <div class="form-group">
                <input type="text" name="diachi" class="form-control" placeholder="Địa chỉ">
              </div>
              <div class="form-group">
                <input type="text" name="coquan" class="form-control" placeholder="Cơ quan công tác">
              </div> -->
              <button type="submit"  class="btn btn-primary block full-width m-b">Đăng nhập</button>
              
              <p class="text-muted text-center"><small>Đã có tài khoản?</small></p>
              <a class="btn btn-sm btn-white btn-block" href="index.php?controller=users&action=login">Đăng nhập</a>
            </form>
            <p class="m-t"> <small>Copyright Bắp&amp;Rốt &copy; 2017</small> </p>
            <p class="m-t"> <small>Đề tài môn học Lập trình mạng</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="views/assets/js/jquery-2.1.1.js"></script>
    <script src="views/assets/js/bootstrap.min.js"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.2/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 12 Jul 2015 09:55:25 GMT -->
</html>
