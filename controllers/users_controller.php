﻿<?php
  class UsersController {

  	public function redirect($url){
			// die("<script>window.location.href = 'http://localhost:8070/ltweb_aboutme/index.php?" . $url . "';</script>");
			die("<script>window.location.href = 'http://localhost/ltweb_aboutme/index.php?" . $url . "';</script>");
		}

    public function edit() {
    	if (isset($_SESSION['user_infor'])){
    		$me = new Me();
	      $_SESSION['user_infor'] = $me->getMeById($_SESSION['user_infor']['Id']);
	      require_once('views/users/edit.php');
    	} else
    		$this->redirect('controller=users&action=login');
    }

    public function login() {
      require_once('views/users/login.php');
    }

		public function postLogin(){
			$me = new Me();
			$email = $_POST['email'];
			$matkhau = $_POST['matkhau'];
			$check = $me->getMeByEmail($email);
			// var_dump($check['MatKhau']);
			if(!$check){
				$_SESSION['warning'] = 1;
				$this->redirect('controller=users&action=login');
			} else if ($check['MatKhau'] != $matkhau){
				$_SESSION['warning'] = 1;
				$this->redirect('controller=users&action=login');
			} else if ($check['MatKhau'] == $matkhau){
				$_SESSION['user_infor'] = $check;				
				$this->redirect('controller=users&action=preview');
			}
			$this->redirect('controller=pages&action=error');
		}

    public function register() {
      require_once('views/users/register.php');
    }

    public function create() {
    	$me = new Me();
			$email = $_POST['email'];
			$matkhau = $_POST['matkhau'];
			$xacnhanmatkhau = $_POST['xacnhanmatkhau'];
			if ($matkhau != $xacnhanmatkhau) {
				$_SESSION['error'] = "Mật khẩu xác nhận không đúng!";
				$this->redirect('controller=users&action=register');
			} else if (($me->getMeByEmail($email))){
				$_SESSION['error'] = "Tài khoản đã tồn tại!";
				echo "Tài khoản đã tồn tại!";
				$this->redirect('controller=users&action=register');
			} else {
				$args = array("email" => $email, "matkhau" => $matkhau);
				$me->addMe($args);
				$_SESSION['success'] = 1;
				$this->redirect('controller=users&action=login');
			}
    }

		public function profile() {
			if (isset($_GET['me'])) {
				$me = new Me();
				$me_info = $me->getMeById($_GET['me']);
				if ($me_info) {
					$_SESSION['me'] = $me_info;
					require_once('views/users/profile.php');
				} else {
					$this->redirect('controller=users&action=login');
				}
			} else{
				$this->redirect('controller=users&action=login');
			}
    }
    
    public function update() {
    	$me = new Me();
    	$Id = $_POST['id'];
    	$HoTen = $_POST['hoten'];
    	$Email = $_POST['email'];
    	$SoDienThoai = $_POST['sodienthoai'];
    	$DiaChi = $_POST['diachi'];
    	$CoQuan = $_POST['coquan'];
    	$ChucVu = $_POST['chucvu'];
    	$SoThich = $_POST['sothich'];
    	$ChamNgon = $_POST['chamngon'];
    	$KyNang = $_POST['kynang'];

    	$args = array('Id' => $Id, 'HoTen' => $HoTen, 'Email' => $Email, 'SoDienThoai' => $SoDienThoai,
    				 'DiaChi' => $DiaChi, 'CoQuan' => $CoQuan, 'ChucVu' => $ChucVu,
    				 'SoThich' => $SoThich, 'ChamNgon' => $ChamNgon, 'KyNang' => $KyNang);
    	$current_user = $me->getMeById($Id);
    	$target_file = $current_user['HinhAnh'];
    	if (!empty($_FILES['hinhanh']['name'])){
	    	$target_dir = "uploads/" . $_POST['id'] . "/";
	    	$files = glob($target_dir."*"); // get all file names
				foreach($files as $file){ // iterate files
				  if(is_file($file))
				    unlink($file); // delete file
				}
	    	if (!file_exists($target_dir)) {
				    mkdir($target_dir, 0777, true);
				}
				$target_file = $target_dir . basename($_FILES['hinhanh']["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
				    $check = getimagesize($_FILES["hinhanh"]["tmp_name"]);
				    if($check !== false) {
				        echo "File is an image - " . $check["mime"] . ".";
				        $uploadOk = 1;
				    } else {
				        echo "File is not an image.";
				        $uploadOk = 0;
				    }
				}
				// Check if file already exists
				if (file_exists($target_file)) {
				    echo "Sorry, file already exists.";
				    $uploadOk = 0;
				}
			
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				    $uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
				    echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
				} else {
				    if (move_uploaded_file($_FILES["hinhanh"]["tmp_name"], $target_file)) {
				    	// echo $target_file;
				    } else {
				        echo "Sorry, there was an error uploading your file.";
				    }
				}
			}
			$args['HinhAnh'] = $target_file;
                        
			$me->editMe($args);
			$current_user = $me->getMeById($Id);
			$_SESSION['user_infor'] = $current_user;
    	$this->redirect('controller=users&action=edit');
    }

    public function logout() {
    	unset($_SESSION['user_infor']);
    	$this->redirect('controller=users&action=login');
    }

    public function preview() {
    	if (isset($_SESSION['user_infor'])){
    		$me = new Me();
	      $_SESSION['user_infor'] = $me->getMeById($_SESSION['user_infor']['Id']);
	      require_once('views/users/preview.php');
    	} else
    		$this->redirect('controller=users&action=login');
    	
    }

  }
?>