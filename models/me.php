<?php
class Me{
    public $table = "me";
   
    public function getListMe($args = []){
        try{
            $conn = Db::getInstance();
            $sql = $db->query('SELECT * FROM '.$this->table);
            $stmt = $conn->prepare($sql);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }catch(PDOException $e){
            return false;
        }
    }
     public function addMe($args = []){
        try {
            $conn = Db::getInstance();
            $sql = "INSERT INTO me(email, matkhau) VALUES (:email, :matkhau)";
            $stmt = $conn->prepare($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':email',$args['email']);
            $stmt->bindParam(':matkhau',$args['matkhau']);	
            $stmt->execute();
            return true;
        }catch(PDOException $e){	    	
            return false;//error 404
        }
    } 
    public function editMe($args = []){
        try {
            $conn = Db::getInstance();
            $sql  = "UPDATE ".$this->table  
                ." SET HoTen =:HoTen,SoDienThoai=:SoDienThoai,DiaChi =:DiaChi,Email =:Email,CoQuan =:CoQuan,"
                ."ChucVu =:ChucVu,HinhAnh =:HinhAnh,SoThich =:SoThich,KyNang =:KyNang,ChamNgon=:ChamNgon"
                ." WHERE Id =:Id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':HoTen',$args['HoTen']);
            $stmt->bindParam(':SoDienThoai',$args['SoDienThoai']);
            $stmt->bindParam(':DiaChi',$args['DiaChi']);
            $stmt->bindParam(':Email',$args['Email']);
            $stmt->bindParam(':CoQuan',$args['CoQuan']);
            $stmt->bindParam(':ChucVu',$args['ChucVu']);
            $stmt->bindParam(':HinhAnh',$args['HinhAnh']);
            $stmt->bindParam(':SoThich',$args['SoThich']);
            $stmt->bindParam(':KyNang',$args['KyNang']);
            $stmt->bindParam(':ChamNgon',$args['ChamNgon']);

            $stmt->bindParam(':Id',$args['Id']);				
            $stmt->execute();
            return true;
        }catch(PDOException $e){	   
            return false;//error 404
        }
    }  
    public function getMeById($id){
        try{
                $conn = Db::getInstance();
                $sql = 'select * from '.$this->table .' where Id = :Id';
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':Id',$id);
                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $result = $stmt->fetchAll();
                if($result){
                        return $result[0];
                }else{
                        return false;
        }
        }catch(PDOException $e){
                return false;
        }
    }
    public function delMe($id){
        try{
                $conn = Db::getInstance();
                $sql = 'delete  from '.$this->table.' where Id = :Id';
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':Id',$id);
                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $result = $stmt->fetchAll();
                if($result){
                        return $result[0];
                }else{
                        return false;
        }
        }catch(PDOException $e){
                return false;
        }
    }

    public function getMeByEmail($email){
      try{
        $conn = Db::getInstance();
        $sql = 'select * from '.$this->table .' where email = :email';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        if($result){
            return $result[0];
        } else {
            return false;
      }
      }catch(PDOException $e){
        echo "PDOException";
              return false;
      }
    }
    // overloading method getUser in php :))))
    public function __call($nameMethod,$args){
      try{
        $conn = $this->connect();
        switch ($nameMethod) {
        case 'getMe':
          switch (count($args)) {
                  case 1:
                          $sql = 'select * from me where Email = :Email';
                          $stmt = $conn->prepare($sql);
                          $stmt->bindParam(':Email',$args[0]);
                          break;
                  case 2:
                          $sql = 'select * from me where Email = :Email and MatKhau = :MatKhau';
                          $stmt = $conn->prepare($sql);
                          $stmt->bindParam(':Email',$args[0]);
                          $stmt->bindParam(':MatKhau',md5($args[1]));
                          break;
                  default:
                          echo 'Incorrect parameter';
                          exit();
                          break;
          }
          $stmt->execute();
          $stmt->setFetchMode(PDO::FETCH_ASSOC);
          $result = $stmt->fetchAll();					
          if($result){
                  unset($result[0]['Password']);
                  return $result[0];
          }else{
                  return 0;
          }
          return $result;
          break;	
        default:
        echo "the method does not exist"; 
        exit();
        break;			
        }
      }catch(PDOException $e){
              return -1;
      }
  }
}
