<?php
  class Db {
    private static $instance = NULL;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance() {
      try {
          $conn = new PDO("mysql:host=localhost;dbname=aboutme;charset=utf8",'root','root');
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
      } catch (Exception $ex) {
           echo 'Ket noi that bai: ' . $ex->getMessage();
      }
      return $conn;
      // if (!isset(self::$instance)) {
      //   $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
      //   self::$instance = new PDO('mysql:host=localhost;dbname=php_mvc', 'root', 'root', $pdo_options);
      // }
      // return self::$instance;
    }
  }
?>