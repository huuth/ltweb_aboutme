<?php
  function call($controller, $action) {
    require_once('controllers/' . $controller . '_controller.php');
    
    switch($controller) {
      case 'pages':
        $controller = new PagesController();
      break;
      case 'posts':
        require_once('models/post.php');
        $controller = new PostsController();
      break;
      case 'users':
        require_once('models/me.php');
        $controller = new UsersController();
      break;
    }

    $controller->{ $action }();
  }

  // we're adding an entry for the new controller and its actions
  $controllers = array('pages' => ['home', 'error', 'about'],
                       'posts' => ['index', 'show'],
                       'users' => ['login', 'register', 'edit', 'profile', 'postLogin', 'create', 'update',
                                   'logout', 'preview']);

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>